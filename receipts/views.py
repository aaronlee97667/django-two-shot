from django.shortcuts import render, redirect
from .models import Receipt, ExpenseCategory, Account
from .forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required()
def list_view(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt": receipt
    }
    return render(request, "receipts/home.html", context)


def create_list(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            form.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form
    }
    return render(request, "receipts/create.html", context)


@login_required()
def category_list(request):
    expenses = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "expenses": expenses
    }
    return render(request, "receipts/expense.html", context)


@login_required()
def account_list(request):
    account_details = Account.objects.filter(owner=request.user)
    context = {
        "account_details": account_details
    }
    return render(request, "receipts/account_detail.html", context)


@login_required()
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            new_category = form.save(False)
            new_category.owner = request.user
            form.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form
    }
    return render(request, "receipts/create_category.html", context)


@login_required()
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            new_account = form.save(False)
            new_account.owner = request.user
            form.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form
    }
    return render(request, "receipts/create_account.html", context)
