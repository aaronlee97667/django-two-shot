from django.urls import path
from .views import login_account, user_logout, signup


urlpatterns = [
    path("login/", login_account, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", signup, name="signup")
]
